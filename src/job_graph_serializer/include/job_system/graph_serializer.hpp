#pragma once

#include <chrono>
#include <nlohmann/json.hpp>
#include <string_view>
#include <vector>


namespace jobs
{
	class node
	{
	public:
		node(std::string label, std::chrono::nanoseconds time_taken);

		[[nodiscard]] auto label() const -> std::string_view;
		[[nodiscard]] auto time_taken() const -> std::chrono::nanoseconds;

	private:
		std::string label_;
		std::chrono::nanoseconds time_taken_;
	};

	class edge
	{
	public:
		edge(int from_node, int to_node);

		[[nodiscard]] auto from_node() const -> int;
		[[nodiscard]] auto to_node() const -> int;

	private:
		int from_node_;
		int to_node_;
	};

	class graph_serializer
	{
	public:
		auto add_node(node node) -> int;
		auto add_edge(edge edge) -> void;

		auto get_json() -> nlohmann::json;

	private:
		std::vector<node> nodes_{};
		std::vector<edge> edges_{};
	};
}