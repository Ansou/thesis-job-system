#include "benchmark.hpp"

#include "job_system/graph_serializer.hpp"
#include "job_system/job_system.hpp"

#include <algorithm>
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <numeric>
#include <optional>
#include <random>
#include <ratio>
#include <string>
#include <vector>


auto jobs::benchmark::get_number(int min, int max, std::default_random_engine& engine) -> int
{
	auto dist = std::uniform_int_distribution<int>{min, max};
	return dist(engine);
}

auto jobs::benchmark::generate_iteration_counts(std::default_random_engine& rng) -> std::array<int, num_jobs>
{
	auto counts = std::array<int, num_jobs>{};
	for(auto i = 0; i < num_jobs; ++i)
	{
		counts.at(i) = get_number(1, max_iterations_per_job, rng);
	}
	return counts;
}

auto jobs::benchmark::generate_dependencies(std::default_random_engine& rng) -> std::array<std::vector<int>, num_jobs>
{
	auto dependencies = std::array<std::vector<int>, num_jobs>{};
	for(auto i = 0; i < num_jobs; ++i)
	{
		generate_dependencies_for_job(rng, i, dependencies.at(i));
	}
	return dependencies;
}

auto jobs::benchmark::generate_dependencies_for_job(
	std::default_random_engine& rng, int existing_jobs, std::vector<int>& dependencies) -> void
{
	auto num_dependencies = get_number(0, static_cast<int>(existing_jobs * dependency_factor), rng);
	for(auto i = 0; i < num_dependencies; ++i)
	{
		auto dependency_index = get_number(0, existing_jobs - 1, rng);
		if(auto iter = std::find(dependencies.begin(), dependencies.end(), dependency_index);
		   iter == dependencies.end())
		{
			dependencies.emplace_back(dependency_index);
		}
	}
}

auto jobs::benchmark::generate_job_graph(
	jobs::scheduler& scheduler,
	std::array<jobs::job_timer, num_jobs>& times,
	std::array<int, num_jobs>& results,
	std::array<int, num_jobs> const& iteration_counts,
	std::array<std::vector<int>, num_jobs> const& all_dependency_indices,
	std::optional<stage_handle> dependency) -> jobs::stage_handle
{
	auto stage_scheduler = jobs::stage_scheduler{scheduler};
	auto all_handles = std::vector<jobs::job_handle>{};
	for(auto i = 0; i < num_jobs; ++i)
	{
		auto num_iterations = iteration_counts.at(i);
		auto const& dependency_indices = all_dependency_indices.at(i);
		auto dependencies = std::vector<jobs::job_handle>{};
		std::for_each(dependency_indices.begin(), dependency_indices.end(), [&dependencies, &all_handles](auto index) {
			dependencies.emplace_back(all_handles.at(index));
		});
		constexpr auto start_value = 1;
		auto& result = results.at(i);
		all_handles.emplace_back(stage_scheduler.add_job(
			[&result, num_iterations, start_value] { result += do_work(num_iterations, start_value); },
			times.at(i).time_taken(),
			&times.at(i),
			{},
			dependencies));
	}
	return dependency.has_value() ? stage_scheduler.schedule_jobs(&dependency.value()) :
									stage_scheduler.schedule_jobs();
}

auto jobs::benchmark::do_work(int num_iterations, int start_value) -> int
{
	auto val = start_value;
	for(auto i = 1; i <= num_iterations; ++i)
	{
		val += i * i * i * i;
	}
	return val;
}

auto jobs::benchmark::run() -> void
{
	auto benchmark_timer = jobs::job_timer{};
	auto times = std::array<std::chrono::steady_clock::duration, num_frames>{};

	jobs::scheduler scheduler{};
	jobs::thread_pool thread_pool{scheduler};

	auto stage_1_times = std::array<jobs::job_timer, num_jobs>{};
	auto stage_2_times = std::array<jobs::job_timer, num_jobs>{};
	auto stage_1_results = std::array<int, num_jobs>{};
	auto stage_2_results = std::array<int, num_jobs>{};

	// auto random_device = std::random_device{};
	// auto seed = random_device();
	auto constexpr seed = 3338395384;
	auto random_engine = std::default_random_engine{seed};
	auto stage_1_iteration_counts = generate_iteration_counts(random_engine);
	auto stage_2_iteration_counts = generate_iteration_counts(random_engine);
	auto stage_1_job_dependencies = generate_dependencies(random_engine);
	auto stage_2_job_dependencies = generate_dependencies(random_engine);
	benchmark_timer.start();

	auto current_frame = 0;
	auto stage_1_handle = generate_job_graph(
		scheduler, stage_1_times, stage_1_results, stage_1_iteration_counts, stage_1_job_dependencies, {});
	auto stage_2_handle = generate_job_graph(
		scheduler, stage_2_times, stage_2_results, stage_2_iteration_counts, stage_2_job_dependencies, stage_1_handle);

	for(current_frame = 1; current_frame < num_frames; ++current_frame)
	{
		stage_1_handle.wait();
		stage_1_handle = generate_job_graph(
			scheduler, stage_1_times, stage_1_results, stage_1_iteration_counts, stage_1_job_dependencies, {});
		stage_2_handle.wait();
		benchmark_timer.finish();
		auto last_frame = current_frame - 1;
		times.at(last_frame) = benchmark_timer.time_taken();
		benchmark_timer.start();
		stage_2_handle = generate_job_graph(
			scheduler,
			stage_2_times,
			stage_2_results,
			stage_2_iteration_counts,
			stage_2_job_dependencies,
			stage_1_handle);
	}
	stage_2_handle.wait();
	benchmark_timer.finish();
	auto last_frame = current_frame - 1;
	times.at(last_frame) = benchmark_timer.time_taken();
	save_results_to_file(times, seed);

	print_job_results(stage_1_results);
	print_job_results(stage_2_results);

	save_graph_to_file(stage_1_times, stage_1_job_dependencies, stage_2_times, stage_2_job_dependencies);
}

auto jobs::benchmark::save_results_to_file(
	std::array<std::chrono::steady_clock::duration, num_frames> const& times, std::random_device::result_type seed)
	-> void
{
	auto directory_path = std::filesystem::path{"../benchmark_results_with_dependencies"} / results_folder;
	std::filesystem::create_directories(directory_path);
	auto file_counts = 0;
	for(auto const& iter : std::filesystem::directory_iterator{directory_path})
	{
		++file_counts;
	}
	auto filename = "result" + std::to_string(file_counts) + ".txt";
	auto file = std::ofstream{directory_path / filename};
	file << "Seed: " << seed << "\n";
	auto sum = std::reduce(times.begin(), times.end(), std::chrono::nanoseconds{}, [](auto&& sum, auto&& current) {
		return sum + current;
	});
	auto average = sum / times.size();
	file << "Total time for " << times.size() << " frames: " << sum << ".\n";
	file << "Average frame time: " << average << ".\n";
	for(auto i = 0; i < times.size(); ++i)
	{
		file << "Frame " << i << " time: " << times.at(i) << ".\n";
	}
}

auto jobs::benchmark::print_job_results(std::array<int, num_jobs> const& results) -> void
{
	auto total = std::reduce(results.begin(), results.end(), 0);
	std::cout << total << "\n";
}

auto jobs::benchmark::build_json_graph(
	std::array<jobs::job_timer, num_jobs> const& stage_1_times,
	std::array<std::vector<int>, num_jobs> const& stage_1_dependencies,
	std::array<jobs::job_timer, num_jobs> const& stage_2_times,
	std::array<std::vector<int>, num_jobs> const& stage_2_dependencies) -> nlohmann::json
{
	auto serializer = graph_serializer{};
	auto offset = 0;
	auto stage_1_start = serializer.add_node({"Stage 1 Start", {}});
	++offset;

	auto has_dependents = std::array<bool, num_jobs>{};
	for(auto current_job_num = 0; current_job_num < num_jobs; ++current_job_num)
	{
		serializer.add_node(
			{"Job " + std::to_string(current_job_num) + ": " +
				 std::to_string(stage_1_times.at(current_job_num).time_taken().count()) + " ns",
			 stage_1_times.at(current_job_num).time_taken()});
		auto const& current_dependencies = stage_1_dependencies.at(current_job_num);
		if(current_dependencies.empty())
		{
			serializer.add_edge({stage_1_start, current_job_num + offset});
		}
		std::for_each(
			current_dependencies.begin(),
			current_dependencies.end(),
			[&serializer, current_job_num, offset, &has_dependents](int from) {
				has_dependents.at(from) = true;
				serializer.add_edge({from + offset, current_job_num + offset});
			});
	}
	auto stage_1_end = serializer.add_node({"Stage 1 End", {}});
	for(auto i = 0; i < has_dependents.size(); ++i)
	{
		if(!has_dependents.at(i))
		{
			serializer.add_edge({i + offset, stage_1_end});
		}
	}
	++offset;
	offset += num_jobs;

	auto stage_2_start = serializer.add_node({"Stage 2 Start", {}});
	++offset;
	serializer.add_edge({stage_1_end, stage_2_start});

	has_dependents = std::array<bool, num_jobs>{};
	for(auto current_job_num = 0; current_job_num < num_jobs; ++current_job_num)
	{
		serializer.add_node(
			{"Job " + std::to_string(current_job_num) + ": " +
				 std::to_string(stage_2_times.at(current_job_num).time_taken().count()) + " ns",
			 stage_2_times.at(current_job_num).time_taken()});
		auto const& current_dependencies = stage_2_dependencies.at(current_job_num);
		if(current_dependencies.empty())
		{
			serializer.add_edge({stage_2_start, current_job_num + offset});
		}
		std::for_each(
			current_dependencies.begin(),
			current_dependencies.end(),
			[&serializer, current_job_num, offset, &has_dependents](int from) {
				has_dependents.at(from) = true;
				serializer.add_edge({from + offset, current_job_num + offset});
			});
	}
	auto stage_2_end = serializer.add_node({"Stage 1 End", {}});
	for(auto i = 0; i < has_dependents.size(); ++i)
	{
		if(!has_dependents.at(i))
		{
			serializer.add_edge({i + offset, stage_2_end});
		}
	}

	return serializer.get_json();
}

auto jobs::benchmark::save_graph_to_file(
	std::array<jobs::job_timer, num_jobs> const& stage_1_times,
	std::array<std::vector<int>, num_jobs> const& stage_1_dependencies,
	std::array<jobs::job_timer, num_jobs> const& stage_2_times,
	std::array<std::vector<int>, num_jobs> const& stage_2_dependencies) -> void
{
	auto directory_path = std::filesystem::path{"../job_graph"};
	std::filesystem::create_directories(directory_path);
	auto filename = std::string{"job_graph.json"};
	auto file_path = directory_path / filename;
	auto file = std::ofstream{file_path};
	auto json = build_json_graph(stage_1_times, stage_1_dependencies, stage_2_times, stage_2_dependencies);
	file << std::setw(4) << json << "\n";
}

auto jobs::benchmark::operator<<(std::ostream& out, std::chrono::nanoseconds time) -> std::ostream&
{
	out << time.count() << " ns (" << std::chrono::duration_cast<std::chrono::milliseconds>(time).count() << " ms)";
	return out;
}
