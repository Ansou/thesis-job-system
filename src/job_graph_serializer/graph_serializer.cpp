#include <chrono>
#include <job_system/graph_serializer.hpp>
#include <nlohmann/json.hpp>
#include <string>
#include <utility>


jobs::node::node(std::string label, std::chrono::nanoseconds time_taken) :
	label_{std::move(label)}, time_taken_{time_taken}
{
}

auto jobs::node::label() const -> std::string_view
{
	return label_;
}

auto jobs::node::time_taken() const -> std::chrono::nanoseconds
{
	return time_taken_;
}

jobs::edge::edge(int from_node, int to_node) : from_node_{from_node}, to_node_{to_node} {}

auto jobs::edge::from_node() const -> int
{
	return from_node_;
}

auto jobs::edge::to_node() const -> int
{
	return to_node_;
}

auto jobs::graph_serializer::add_node(node node) -> int
{
	nodes_.emplace_back(node);
	return static_cast<int>(nodes_.size() - 1);
}

auto jobs::graph_serializer::add_edge(edge edge) -> void
{
	edges_.emplace_back(edge);
}

auto jobs::graph_serializer::get_json() -> nlohmann::json
{
	auto json = nlohmann::json{};
	auto graph = nlohmann::json{};
	graph["directed"] = true;
	graph["type"] = "job_graph";
	graph["label"] = "Job Graph";
	auto nodes = nlohmann::json{};
	for(auto num = 0; num < nodes_.size(); ++num)
	{
		auto&& node = nodes_.at(num);
		auto node_json = nlohmann::json{};
		node_json["id"] = std::to_string(num);
		node_json["type"] = "job";
		node_json["label"] = node.label();
		node_json["metadata"] = {{"time_taken", node.time_taken().count()}};
		nodes.emplace_back(node_json);
	}
	auto edges = nlohmann::json{};
	for(auto&& edge : edges_)
	{
		auto edge_json = nlohmann::json{};
		edge_json["source"] = std::to_string(edge.from_node());
		edge_json["target"] = std::to_string(edge.to_node());
		edge_json["relation"] = "dependency";
		edge_json["directed"] = true;
		edges.emplace_back(edge_json);
	}
	graph["nodes"] = nodes;
	graph["edges"] = edges;
	json["graph"] = graph;
	return json;
}
