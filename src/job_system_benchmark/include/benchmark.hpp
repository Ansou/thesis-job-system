#pragma once

#include <array>
#include <chrono>
#include <job_system/graph_serializer.hpp>
#include <job_system/job_system.hpp>
#include <optional>
#include <ostream>
#include <random>
#include <string_view>
#include <vector>


namespace jobs::benchmark
{
	constexpr auto num_frames = 1000;
	constexpr auto num_jobs = 1000;
	constexpr auto dependency_factor = 0.1;
	int constexpr max_iterations_per_job = 500000;
	std::string_view constexpr results_folder = "with_overlap_with_priority";

	auto get_number(int min, int max, std::default_random_engine& engine) -> int;
	auto generate_iteration_counts(std::default_random_engine& rng) -> std::array<int, num_jobs>;
	auto generate_dependencies(std::default_random_engine& rng) -> std::array<std::vector<int>, num_jobs>;
	auto generate_dependencies_for_job(
		std::default_random_engine& rng, int existing_jobs, std::vector<int>& dependencies) -> void;
	auto generate_job_graph(
		jobs::scheduler& scheduler,
		std::array<jobs::job_timer, num_jobs>& times,
		std::array<int, num_jobs>& results,
		std::array<int, num_jobs> const& iteration_counts,
		std::array<std::vector<int>, num_jobs> const& all_dependency_indices,
		std::optional<stage_handle>) -> jobs::stage_handle;
	auto do_work(int num_iterations, int start_value) -> int;
	auto run() -> void;
	auto save_results_to_file(
		std::array<std::chrono::steady_clock::duration, num_frames> const& times, std::random_device::result_type seed)
		-> void;
	auto print_job_results(std::array<int, num_jobs> const& results) -> void;
	auto build_json_graph(
		std::array<jobs::job_timer, num_jobs> const& stage_1_times,
		std::array<std::vector<int>, num_jobs> const& stage_1_dependencies,
		std::array<jobs::job_timer, num_jobs> const& stage_2_times,
		std::array<std::vector<int>, num_jobs> const& stage_2_dependencies) -> nlohmann::json;
	auto save_graph_to_file(
		std::array<jobs::job_timer, num_jobs> const& stage_1_times,
		std::array<std::vector<int>, num_jobs> const& stage_1_dependencies,
		std::array<jobs::job_timer, num_jobs> const& stage_2_times,
		std::array<std::vector<int>, num_jobs> const& stage_2_dependencies) -> void;

	auto operator<<(std::ostream& out, std::chrono::nanoseconds time) -> std::ostream&;
}
