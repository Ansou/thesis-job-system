#include "benchmark.hpp"

#include <iostream>


auto main() -> int
{
	jobs::benchmark::run();
}
