#include <chrono>
#include <vector>
#include <numeric>
#include <string>
#include <array>

#include <job_system/job_system.hpp>
#include <catch2/catch.hpp>
#include <iostream>

auto frame_time_timeout() -> std::chrono::nanoseconds
{
	return std::chrono::nanoseconds{ std::chrono::seconds{1} } / 60;
}

TEST_CASE("Run simple job", "[scheduler]")
{
	auto i = 0;
	jobs::scheduler scheduler{};
	auto new_job = scheduler.add_job([&i] {++i; }, {});
	scheduler.execute_next_job(frame_time_timeout());
	REQUIRE(i == 1);
}

TEST_CASE("Run job in thread pool with single thread", "[scheduler]")
{
	auto i = 0;
	jobs::scheduler scheduler{};
	scheduler.add_job([&i] {++i; }, {});
	{
		jobs::thread_pool thread_pool { scheduler, frame_time_timeout(), 1 };
	}
	REQUIRE(i == 1);
}

TEST_CASE("Run parallel jobs", "[scheduler]")
{
	auto vec_size = 1000;
	auto iterations = 1000;
	auto vec = std::vector<int>(vec_size);
	jobs::scheduler scheduler {};
	for (auto i = 0; i < vec.size(); ++i)
	{
		scheduler.add_job([&vec, i, iterations] {for (auto j = 0; j < iterations; ++j) { ++vec[i]; }}, {});
	}
	{
		jobs::thread_pool thread_pool{ scheduler };
	}
	REQUIRE(std::accumulate(vec.begin(), vec.end(), 0) == vec_size * iterations);
}

TEST_CASE("Run jobs with dependencies", "[scheduler]")
{
	auto i = 0;
	auto num_jobs = 100;
	jobs::scheduler scheduler {};
	auto prev_handle = scheduler.add_job([&i] {++i; }, {});
	for (auto j = 1; j < num_jobs; ++j)
	{
		prev_handle = scheduler.add_job([&i] {++i; }, {}, { prev_handle });
	}
	{
		jobs::thread_pool thread_pool{ scheduler};
	}
	REQUIRE(i == num_jobs);
}

//struct simulation_jobs_time
//{
//	jobs::job_timer input_polling{};
//	jobs::job_timer player_controls{};
//	jobs::job_timer gui_update{};
//	jobs::job_timer camera_update{};
//	jobs::job_timer ai_update{};
//	jobs::job_timer objects_move{};
//	jobs::job_timer collision{};
//};
//
//struct render_jobs_time
//{
//	jobs::job_timer culling{};
//	jobs::job_timer render_objects{};
//};
//
//struct frame_param
//{
//	std::string hello{};
//};
//
//auto schedule_simulation_jobs(simulation_jobs_time& sim_jobs_time, jobs::scheduler& schedule, frame_param& frame_params) -> jobs::stage_handle
//{
//	auto stage = jobs::stage_scheduler{ schedule };
//	auto input_polling_handle = stage.add_job([&frame_params] {}, &sim_jobs_time.input_polling, {});
//	auto player_controls_handle = stage.add_job([] {}, &sim_jobs_time.player_controls, {}, { input_polling_handle });
//	auto gui_update_handle = stage.add_job([] {}, &sim_jobs_time.gui_update, {}, { input_polling_handle });
//	auto camera_update_handle = stage.add_job([] {}, &sim_jobs_time.camera_update, {}, { input_polling_handle });
//	auto ai_update_handle = stage.add_job([] {}, &sim_jobs_time.ai_update, {});
//	auto objects_move_handle = stage.add_job([] {}, &sim_jobs_time.objects_move, {}, { player_controls_handle, camera_update_handle, ai_update_handle });
//	auto collision_handle = stage.add_job([] {}, &sim_jobs_time.collision, {}, { objects_move_handle });
//
//	return stage.schedule_jobs();
//}
//
//auto schedule_render_jobs(render_jobs_time& ren_jobs_time, jobs::scheduler& scheduler, jobs::stage_handle previous_stage, frame_param& frame_params) -> jobs::stage_handle
//{
//	auto stage = jobs::stage_scheduler{scheduler};
//	auto culling_handle = stage.add_job([] {}, &ren_jobs_time.culling, {});
//	auto render_objects_handle = stage.add_job([] {}, &ren_jobs_time.render_objects, {}, { culling_handle });
//
//	return stage.schedule_jobs(previous_stage);
//}
//
//auto main_loop() -> void
//{
//	auto scheduler = jobs::scheduler{};
//	auto thread_pool = jobs::thread_pool{ scheduler };
//
//	constexpr auto max_frames_in_flight = 2;
//	auto frame_params = std::array<frame_param, max_frames_in_flight>{};
//
//	auto sim_jobs_time = simulation_jobs_time{};
//	auto ren_jobs_time = render_jobs_time{};
//
//	auto current_frame = 0;
//	auto& current_frame_param = frame_params[current_frame % 2];
//
//	auto simulation_stage_handle = schedule_simulation_jobs(sim_jobs_time, scheduler, current_frame_param);
//	auto render_stage_handle = schedule_render_jobs(ren_jobs_time, scheduler, simulation_stage_handle, current_frame_param);
//
//	for(current_frame = 1; current_frame < 10; ++current_frame)
//	{
//		current_frame_param = frame_params[current_frame % 2];
//
//		simulation_stage_handle.wait();
//		simulation_stage_handle = schedule_simulation_jobs(sim_jobs_time, scheduler, current_frame_param);
//		render_stage_handle.wait();
//		render_stage_handle = schedule_render_jobs(ren_jobs_time, scheduler, simulation_stage_handle, current_frame_param);
//	}
//}

TEST_CASE("Run jobs in stages", "[scheduler]")
{
	jobs::scheduler scheduler {};
	jobs::thread_pool thread_pool{ scheduler };

	auto job_1_time = jobs::job_timer{};
	auto job_2_time = jobs::job_timer{};
	auto job_3_time = jobs::job_timer{};
	auto job_4_time = jobs::job_timer{};

	auto var1 = 0;
	auto var2 = 0;
	auto stage_1_scheduler = jobs::stage_scheduler{ scheduler };
	stage_1_scheduler.add_job([&var1] {++var1; }, job_1_time.time_taken(), &job_1_time, {});
	stage_1_scheduler.add_job([&var2] {var2 += 2; }, job_2_time.time_taken(), &job_2_time, {});
	auto stage_1_handle = stage_1_scheduler.schedule_jobs();

	auto stage_2_scheduler = jobs::stage_scheduler{ scheduler };
	stage_2_scheduler.add_job([&var1] {var1 += 3; }, job_3_time.time_taken(), &job_3_time, {});
	stage_2_scheduler.add_job([&var2] {var2 += 4; }, job_4_time.time_taken(), &job_4_time, {});
	auto const stage_2_handle = stage_2_scheduler.schedule_jobs(&stage_1_handle);
	
	stage_2_handle.wait();

	REQUIRE(var1 == 4);
	REQUIRE(var2 == 6);
}

constexpr int data_parallel_jobs = 100;

struct test_frame_param
{
	jobs::resource<int> first_int{};
	std::vector<jobs::resource<int>> second_vars = std::vector<jobs::resource<int>>( data_parallel_jobs );
	jobs::resource<int> result{};
};

struct stage_1_jobs_time
{
	jobs::job_timer job_1{};
	jobs::job_timer jobs_2{};
};

struct stage_2_jobs_time
{
	jobs::job_timer job_1{};
};

auto schedule_stage_1_jobs(stage_1_jobs_time& stage_1_jobs_time, jobs::scheduler& schedule, test_frame_param& frame_params, int current_frame) -> jobs::stage_handle
{
	auto stage = jobs::stage_scheduler{ schedule };
	auto job_1_policy = jobs::access_policy_container{};
	job_1_policy.add_policy(frame_params.first_int.handle(), { jobs::access_mode::write });
	auto const job_1 = stage.add_job([first_int = frame_params.first_int.handle(), current_frame] {first_int.write() = current_frame; }, stage_1_jobs_time.job_1.time_taken(), &stage_1_jobs_time.job_1, job_1_policy);
	auto const job_2 = stage.add_for_job(data_parallel_jobs,
	                                     [&frame_params](auto job_num)
	                                     {
		                                     return std::make_tuple(frame_params.first_int.handle(), frame_params.second_vars.at(job_num).handle(), job_num);
	                                     },
	                                     [](auto&& resource_tuple)
	                                     {
											 auto&& first_var = std::get<0>(resource_tuple);
											 auto&& second_var = std::get<1>(resource_tuple);
											 auto&& job_num = std::get<2>(resource_tuple);
		                                     //const auto& [first_var, second_var, job_num] = resource_tuple;
		                                     second_var.write() = job_num * first_var.read();
	                                     },
										stage_1_jobs_time.jobs_2.time_taken() ,
	                                     &stage_1_jobs_time.jobs_2,
	                                     [](auto&& resource_tuple)
	                                     {
											 auto&& first_var = std::get<0>(resource_tuple);
											 auto&& second_var = std::get<1>(resource_tuple);
											 auto&& job_num = std::get<2>(resource_tuple);
		                                     //const auto& [first_var, second_var, job_num] = resource_tuple;
		                                     auto policy = jobs::access_policy_container{};
		                                     policy.add_policy(first_var, { jobs::access_mode::read });
		                                     policy.add_policy(second_var, { jobs::access_mode::write });
		                                     return policy;
	                                     },
	                                     {job_1});

	return stage.schedule_jobs();
}

auto schedule_stage_2_jobs(stage_2_jobs_time& stage_2_jobs_time, jobs::scheduler& scheduler, jobs::stage_handle previous_stage, test_frame_param& frame_params) -> jobs::stage_handle
{
	auto stage = jobs::stage_scheduler{ scheduler };
	auto job_1_policy = jobs::access_policy_container{};
	job_1_policy.add_policy(frame_params.result.handle(), { jobs::access_mode::write });
	std::for_each(frame_params.second_vars.begin(), frame_params.second_vars.end(), [&job_1_policy](auto&& resouce) {job_1_policy.add_policy(resouce.handle(), { jobs::access_mode::read }); });
	auto job_1 = stage.add_job([result = frame_params.result.handle(), &second_vars = frame_params.second_vars]{ result.write() = std::accumulate(second_vars.begin(), second_vars.end(), 0, [](auto&& existing_value, auto&& resource) {return resource.handle().read() + existing_value; }); }, stage_2_jobs_time.job_1.time_taken(), &stage_2_jobs_time.job_1, job_1_policy);

	return stage.schedule_jobs(&previous_stage);
}

auto run_jobs(jobs::scheduler& scheduler) -> void
{
	while(scheduler.execute_next_job(std::chrono::nanoseconds{0}))
	{
	}
}

TEST_CASE("Run jobs in frame loop", "[scheduler]")
{
	auto constexpr num_frames = 60;
	auto oracle = std::array<int, num_frames>{};
	for(auto i = 0; i < num_frames; ++i)
	{
		for(auto j = 0; j < data_parallel_jobs; ++j)
		{
			oracle[i] += i * j;
		}
	}
	
	jobs::scheduler scheduler{};
	jobs::thread_pool thread_pool { scheduler};

	constexpr auto max_frames_in_flight = 2;
	auto frame_params = std::array<test_frame_param, max_frames_in_flight>{};

	auto test_read_policies = std::array<jobs::access_policy_container, max_frames_in_flight>{};
	for(auto i = 0; i < max_frames_in_flight; ++i)
	{
		test_read_policies[i].add_policy(frame_params[i].result.handle(), { jobs::access_mode::read });
	}

	auto stage_1_time = stage_1_jobs_time{};
	auto stage_2_time = stage_2_jobs_time{};

	auto current_frame = 0;
	auto& current_frame_param = frame_params[current_frame % 2];

	auto stage_1_handle = schedule_stage_1_jobs(stage_1_time, scheduler, current_frame_param, current_frame);
	auto stage_2_handle = schedule_stage_2_jobs(stage_2_time, scheduler, stage_1_handle, current_frame_param);

	for (current_frame = 1; current_frame < num_frames; ++current_frame)
	{
		auto& loop_current_frame_param = frame_params[current_frame % 2];
		stage_1_handle.wait();
		stage_1_handle = schedule_stage_1_jobs(stage_1_time, scheduler, loop_current_frame_param, current_frame);
		stage_2_handle.wait();
		auto last_frame = current_frame - 1;
		test_read_policies[last_frame % 2].activate_policies();
		REQUIRE(frame_params[last_frame % 2].result.handle().read() == oracle[last_frame]);
		test_read_policies[last_frame % 2].reset_policies();
		stage_2_handle = schedule_stage_2_jobs(stage_2_time, scheduler, stage_1_handle, loop_current_frame_param);
	}
	stage_2_handle.wait();
	auto last_frame = current_frame - 1;
	test_read_policies[last_frame % 2].activate_policies();
	REQUIRE(frame_params[last_frame % 2].result.handle().read() == oracle[last_frame]);
	test_read_policies[last_frame % 2].reset_policies();
}

TEST_CASE("Parallel for job", "[scheduler]")
{
	constexpr auto num_jobs = 100;
	auto resource_container = std::array<int, num_jobs>{};

	jobs::scheduler scheduler {};
	jobs::thread_pool thread_pool { scheduler };

	auto const job_handle = scheduler.add_for_job(num_jobs, 
	                                              [&resource_container](auto job_num) { return jobs::resource_handle<int>{ resource_container.at(job_num) }; }, 
	                                              [](auto resource) { resource.write() += 1; }, 
	                                              [](auto&& resource) { auto policies = jobs::access_policy_container{}; policies.add_policy(resource, jobs::access_policy{ jobs::access_mode::write }); return policies; });

	job_handle.wait();

	for(auto elem : resource_container)
	{
		REQUIRE(elem == 1);
	}
}

TEST_CASE("Parallel for job in frame stage", "[scheduler]")
{
	jobs::scheduler scheduler {};
	jobs::thread_pool thread_pool { scheduler };

	constexpr auto num_jobs = 100;
	auto resource_container = std::array<int, num_jobs>{};
	auto job_1_time = jobs::job_timer{};

	auto stage_1_scheduler = jobs::stage_scheduler{ scheduler };
	stage_1_scheduler.add_for_job(num_jobs,
	                              [&resource_container](auto job_num) { return jobs::resource_handle<int>{ resource_container.at(job_num) }; },
	                              [](auto resource) { resource.write() += 1; },
	                              job_1_time.time_taken(), &job_1_time, [](auto&& resource) { auto policies = jobs::access_policy_container{}; policies.add_policy(resource, jobs::access_policy{ jobs::access_mode::write }); return policies; }
	);
	auto const stage_1_handle = stage_1_scheduler.schedule_jobs();
	stage_1_handle.wait();

	for (auto elem : resource_container)
	{
		REQUIRE(elem == 1);
	}
}

TEST_CASE("Test resource read", "[handles]")
{
	auto resource = jobs::resource<int>{ 5 };
	auto const handle = resource.handle();
	auto const policy = jobs::access_policy{ jobs::access_mode::read };
	auto policies = jobs::access_policy_container{};
	policies.add_policy(handle, policy);
	policies.activate_policies();
	auto&& ref = handle.read();
	REQUIRE(ref == 5);
	policies.reset_policies();
}

TEST_CASE("Test resource write", "[handles]")
{
	auto resource = jobs::resource<int>{ 5 };
	auto handle = resource.handle();
	auto const policy = jobs::access_policy{ jobs::access_mode::write };
	auto policies = jobs::access_policy_container{};
	policies.add_policy(handle, policy);
	policies.activate_policies();
	auto&& ref = handle.write();
	ref += 3;
	REQUIRE(ref == 8);
	policies.reset_policies();
}

TEST_CASE("Job resource read", "[handles]")
{
	auto resource = jobs::resource<int>{ 5 };
	auto handle = resource.handle();
	auto result = 0;
	jobs::scheduler scheduler {};
	jobs::thread_pool thread_pool {scheduler};
	auto policies = jobs::access_policy_container{};
	policies.add_policy(handle, { jobs::access_mode::read });
	scheduler.add_job([handle, &result] {auto&& ref = handle.read(); result = ref * 2; }, policies);
	thread_pool.stop();
	REQUIRE(result == 10);
}

TEST_CASE("Job resource read and write", "[handles]")
{
	auto resource_1 = jobs::resource<int>{ 5 };
	auto resource_2 = jobs::resource<int>{ 0 };
	auto handle_1 = resource_1.handle();
	auto handle_2 = resource_2.handle();
	jobs::scheduler scheduler {};
	jobs::thread_pool thread_pool { scheduler };
	auto policies = jobs::access_policy_container{};
	policies.add_policy(handle_1, { jobs::access_mode::read });
	policies.add_policy(handle_2, { jobs::access_mode::write });
	scheduler.add_job([handle_1, handle_2] { handle_2.write() = handle_1.read() * 2; }, policies);
	thread_pool.stop();
	auto local_policy = jobs::access_policy_container{};
	local_policy.add_policy(handle_2, { jobs::access_mode::read });
	local_policy.activate_policies();
	REQUIRE(handle_2.read() == 10);
	local_policy.reset_policies();
}

TEST_CASE("Test resource access overlap", "[handles]")
{
	auto result_2 = 0;
	auto resource = jobs::resource<int>{ 5 };
	auto handle = resource.handle();
	jobs::scheduler scheduler {};
	jobs::thread_pool thread_pool { scheduler };
	auto policies_1 = jobs::access_policy_container{};
	policies_1.add_policy(handle, { jobs::access_mode::write });
	auto policies_2 = jobs::access_policy_container{};
	policies_2.add_policy(handle, { jobs::access_mode::read });
	auto job_1 = scheduler.add_job([handle] {handle.write() *= 2; }, policies_1);
	scheduler.add_job([handle, &result_2] {result_2 = handle.read() * 3; }, policies_2, {job_1});
	thread_pool.stop();
	REQUIRE(result_2 == 30);
}
