@echo off
mkdir "Visual Studio"
cd "Visual Studio"
conan install .. -s build_type=Release --build missing
cmake .. -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -G "Visual Studio 16 2019"
