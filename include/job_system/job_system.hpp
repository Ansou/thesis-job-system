#pragma once

#include <atomic>
#include <cassert>
#include <chrono>
#include <functional>
#include <map>
#include <queue>
#include <shared_mutex>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <vector>


#ifndef NDEBUG
#define ENABLE_ACCESS_OVERLAP_CHECKS
#endif


namespace jobs
{
#ifdef ENABLE_ACCESS_OVERLAP_CHECKS
	constexpr bool enable_access_overlap_checks = true;
#else
	constexpr bool enable_access_overlap_checks = false;
#endif

	class scheduler;
	class job;
	class stage_scheduler;
	class access_policy_container;

	enum class access_mode
	{
		none,
		read,
		write
	};

	struct access_policy
	{
		access_mode access_mode{};

		[[nodiscard]] auto overlaps(access_policy other) const -> bool;
	};

	class policy_storage
	{
	protected:
		static thread_local std::unordered_map<uintptr_t, access_policy> policies_;

	private:
		static auto reset_policies() -> void;
		static auto set_policy(uintptr_t ptr, access_policy policy) -> void;

		friend class access_policy_container;
	};

	template<typename T>
	class resource_handle : public policy_storage
	{
	public:
		// NOTE: In a real game, this resource handle would likely use an index rather than a pointer.
		explicit resource_handle(T& resource);

		auto read() const -> T const&;
		auto write() const -> T&;

	private:
		T* resource_;

		friend class access_policy_container;
	};

	template<typename T>
	class resource
	{
	public:
		template<typename... Args>
		explicit resource(Args&&... args);

		auto handle() -> resource_handle<T>;

	private:
		T object_;
	};

	class access_policy_container
	{
	public:
		template<typename ResourceType>
		auto add_policy(resource_handle<ResourceType> handle, access_policy policy) -> void;

		auto activate_policies() -> void;
		auto reset_policies() -> void;

	private:
		std::vector<std::pair<uintptr_t, access_policy>> access_policies_{};

		friend class scheduler;
		friend class stage_scheduler;
	};

	class job_handle
	{
	public:
		auto operator<(job_handle const& other) const -> bool;
		auto operator>(job_handle const& other) const -> bool;

		auto wait() const -> void;

	private:
		job_handle(scheduler& scheduler, size_t id, std::chrono::nanoseconds previous_time);

		auto get_job() const -> job*;

	private:
		scheduler* scheduler_;
		size_t id_;
		std::chrono::nanoseconds previous_time_;

		friend class job;
		friend class scheduler;
		friend class stage_scheduler;
	};

	class stage_handle
	{
	public:
		auto wait() const -> void;

	private:
		explicit stage_handle(job_handle end_job_handle);

	private:
		job_handle end_job_handle_;

		friend class stage_scheduler;
		friend class scheduler;
	};

	class job_timer
	{
	public:
		auto start() -> void;
		auto finish() -> void;
		auto time_taken() const -> std::chrono::steady_clock::duration;

	private:
		std::chrono::steady_clock::time_point start_time_{};
		std::chrono::steady_clock::duration time_taken_{};
	};

	class job
	{
	public:
		job(std::function<void()> callback,
			scheduler& scheduler,
			size_t id,
			std::vector<job_handle> const& dependencies,
			access_policy_container access_policies,
			std::chrono::nanoseconds previous_time,
			job_timer* timer);

		auto finish() -> void;
		auto decrement_remaining_dependencies() -> void;
		auto push_to_queue() const -> void;
		auto create_handle() const -> job_handle;
		auto register_dependencies(std::vector<job_handle> const& dependencies) -> void;
		auto dependencies() const -> std::vector<job_handle> const&;
		auto policies() const -> access_policy_container const&;
		auto wait() -> void;

		auto operator()() -> void;

		auto operator==(job const& other) const -> bool;

	private:
		std::function<void()> callback_;
		scheduler* scheduler_;
		size_t id_;

		std::atomic<int> dependencies_remaining_;
		std::mutex finish_mutex_{};
		std::vector<job*> dependent_jobs_{}; // Protected by finish_mutex_.
		bool has_finished_{}; // Protected by finish_mutex_.
		std::chrono::steady_clock::time_point finish_time_{}; // Protected by finish_mutex_.
		std::condition_variable finish_signaller_{};

		std::vector<job_handle> dependencies_; // Only used for finding synchronization errors.

		std::chrono::nanoseconds previous_time_;
		job_timer* timer_;

		access_policy_container access_policies_;
	};

	struct job_data
	{
		std::function<void()> callback;
		std::chrono::nanoseconds previous_time;
		job_timer* timer;
		std::vector<job_handle> dependencies;
		size_t id;
		access_policy_container access_policies;
	};

	class stage_scheduler
	{
	public:
		explicit stage_scheduler(scheduler& scheduler);

		auto add_job(
			std::function<void()> callback,
			std::chrono::steady_clock::duration previous_time,
			job_timer* timer,
			access_policy_container access_policies) -> job_handle;
		auto add_job(
			std::function<void()> callback,
			std::chrono::steady_clock::duration previous_time,
			job_timer* timer,
			access_policy_container access_policies,
			std::vector<job_handle> const& dependencies) -> job_handle;

		template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
		auto add_for_job(
			size_t num_jobs,
			ResourceCallback resource_callback,
			JobCallback job_callback,
			std::chrono::steady_clock::duration previous_time,
			job_timer* timer,
			PolicyCallback policy_callback) -> job_handle;
		template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
		auto add_for_job(
			size_t num_jobs,
			ResourceCallback resource_callback,
			JobCallback job_callback,
			std::chrono::steady_clock::duration previous_time,
			job_timer* timer,
			PolicyCallback policy_callback,
			std::vector<job_handle> const& dependencies) -> job_handle;

		auto schedule_jobs(stage_handle* previous_stage = nullptr) -> stage_handle;

	private:
		scheduler* scheduler_;
		std::map<size_t, job_data>
			jobs_{}; // Must be a sorted map, to ensure that jobs get scheduled in the right order.
		// But what if id wraps around? Will cause a bug.
		std::vector<job_handle> handles_{};
	};

	class scheduler
	{
	public:
		auto add_job(std::function<void()> callback, access_policy_container access_policies) -> job_handle;
		auto add_job(
			std::function<void()> callback,
			access_policy_container access_policies,
			std::vector<job_handle> const& dependencies) -> job_handle;

		template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
		auto add_for_job(
			size_t num_jobs,
			ResourceCallback&& resource_callback,
			JobCallback&& job_callback,
			PolicyCallback&& policy_callback) -> job_handle;
		template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
		auto add_for_job(
			size_t num_jobs,
			ResourceCallback&& resource_callback,
			JobCallback&& job_callback,
			PolicyCallback&& policy_callback,
			std::vector<job_handle> const& dependencies) -> job_handle;

		auto execute_next_job(std::chrono::nanoseconds wait_timeout) -> bool;

	private:
		auto create_new_id() -> size_t;
		auto schedule_job(
			std::function<void()>&& callback,
			size_t id,
			const std::vector<job_handle>& dependencies,
			const access_policy_container& access_policies,
			std::chrono::nanoseconds previous_time,
			job_timer* timer) -> job&;
		auto check_access_overlaps(
			std::vector<job_handle> const& dependencies, jobs::access_policy_container const& access_policies) -> void;
		auto check_access_overlap_against_job(
			jobs::access_policy policy,
			jobs::access_policy other_policy,
			job const& other_job,
			std::vector<size_t> const& safe_jobs) const -> void;
		auto find_recursive_dependencies(std::vector<job_handle> const& dependencies) const -> std::vector<size_t>;
		auto push_dependencies_to_stack(
			job_handle job, std::stack<job_handle>& stack, std::vector<size_t> const& visited_jobs) const -> void;
		auto clean_jobs_recursively(job_handle root_job) -> void;
		auto remove_job(size_t id) -> void;
		auto remove_job_policy_from_access_map(job const& job, uintptr_t resource) -> void;

		template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
		static auto process_jobs_in_batch(
			size_t job_offset,
			size_t batch_size,
			ResourceCallback&& resource_callback,
			PolicyCallback&& policy_callback,
			JobCallback&& job_callback,
			std ::unordered_multimap<uintptr_t, access_policy>& access_map)
			-> std::pair<std::vector<std::function<void()>>, access_policy_container>;

		static auto check_access_overlap_parallel_for(
			std::unordered_multimap<uintptr_t, access_policy> const& resource_access_map,
			access_policy_container const& policies) -> void;

	private:
		std::shared_mutex jobs_mutex_{};
		std::unordered_map<size_t, job> all_jobs_{}; // Protected by jobs_mutex_.
		std::recursive_mutex queue_mutex_{};
		std::condition_variable_any queue_cond_var_{};
		std::priority_queue<job_handle, std::vector<job_handle>, std::greater<>>
			job_queue_{}; // Protected by queue_mutex_.
		std::atomic<size_t> current_id_{};

		std::unordered_multimap<uintptr_t, std::pair<job*, access_policy>> resource_access_map_{};


		friend class job;
		friend class job_handle;
		friend class stage_scheduler;
		friend class stage_handle;
	};

	class thread_pool
	{
	public:
		explicit thread_pool(
			scheduler& scheduler,
			std::chrono::nanoseconds queue_wait_timeout = std::chrono::nanoseconds{std::chrono::seconds{1}} / 60,
			int num_threads = std::thread::hardware_concurrency() > 0 ?
				static_cast<int>(std::thread::hardware_concurrency()) :
				1);

		thread_pool(thread_pool const& other) = delete;
		thread_pool(thread_pool&& other) = delete;

		auto operator=(thread_pool const& other) -> thread_pool& = delete;
		auto operator=(thread_pool&& other) -> thread_pool& = delete;

		~thread_pool();

		auto stop() -> void;

	private:
		std::vector<std::thread> threads_{};
		std::atomic<bool> running_{true};
	};
}

template<typename T>
jobs::resource_handle<T>::resource_handle(T& resource) : resource_{&resource}
{
}

template<typename T>
auto jobs::resource_handle<T>::read() const -> T const&
{
	auto const iter = policies_.find(reinterpret_cast<uintptr_t>(resource_));
	assert(iter != policies_.end());
	assert(iter->second.access_mode == access_mode::read || iter->second.access_mode == access_mode::write);
	return *resource_;
}

template<typename T>
auto jobs::resource_handle<T>::write() const -> T&
{
	auto const iter = policies_.find(reinterpret_cast<uintptr_t>(resource_));
	assert(iter != policies_.end());
	assert(iter->second.access_mode == access_mode::write);
	return *resource_;
}

template<typename T>
template<typename... Args>
jobs::resource<T>::resource(Args&&... args) : object_{std::forward<Args>(args)...}
{
}

template<typename T>
auto jobs::resource<T>::handle() -> resource_handle<T>
{
	return resource_handle<T>{object_};
}

template<typename ResourceType>
auto jobs::access_policy_container::add_policy(resource_handle<ResourceType> handle, access_policy policy) -> void
{
	access_policies_.emplace_back(reinterpret_cast<uintptr_t>(handle.resource_), policy);
}

template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
auto jobs::stage_scheduler::add_for_job(
	size_t num_jobs,
	ResourceCallback resource_callback,
	JobCallback job_callback,
	std::chrono::steady_clock::duration previous_time,
	job_timer* timer,
	PolicyCallback policy_callback) -> job_handle
{
	return add_for_job(num_jobs, resource_callback, job_callback, previous_time, timer, policy_callback, {});
}

template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
auto jobs::stage_scheduler::add_for_job(
	size_t num_jobs,
	ResourceCallback resource_callback,
	JobCallback job_callback,
	std::chrono::steady_clock::duration previous_time,
	job_timer* timer,
	PolicyCallback policy_callback,
	std::vector<job_handle> const& dependencies) -> job_handle
{
	auto start_job = add_job(
		[timer] {
			if(timer)
			{
				timer->start();
			}
		},
		previous_time,
		nullptr,
		{},
		dependencies);

	if(num_jobs == 0)
	{
		return add_job(
			[timer] {
				if(timer)
				{
					timer->finish();
				}
			},
			previous_time,
			nullptr,
			{},
			{start_job});
	}

	auto const num_threads = std::thread::hardware_concurrency();
	auto const num_batches = std::min(static_cast<size_t>(num_threads), num_jobs);
	auto const batch_size = num_jobs / num_batches;
	auto const last_batch_size = batch_size + num_jobs % num_batches;

	auto local_resource_access_map = std::unordered_multimap<uintptr_t, jobs::access_policy>{};
	auto job_handles = std::vector<job_handle>{};
	for(auto current_batch = size_t{0}; current_batch < num_batches; ++current_batch)
	{
		auto pair = scheduler::process_jobs_in_batch(
			current_batch * batch_size,
			current_batch == num_batches - 1 ? last_batch_size : batch_size,
			resource_callback,
			policy_callback,
			job_callback,
			local_resource_access_map);
		auto&& callbacks = pair.first;
		auto&& batch_policies = pair.second;
		job_handles.emplace_back(add_job(
			[callbacks] { std::for_each(callbacks.begin(), callbacks.end(), [](auto&& callback) { callback(); }); },
			previous_time,
			nullptr,
			batch_policies,
			{start_job}));
	}

	auto end_job = add_job(
		[timer] {
			if(timer)
			{
				timer->finish();
			}
		},
		previous_time,
		nullptr,
		{},
		job_handles);
	return end_job;
}

template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
auto jobs::scheduler::add_for_job(
	size_t num_jobs, ResourceCallback&& resource_callback, JobCallback&& job_callback, PolicyCallback&& policy_callback)
	-> job_handle
{
	return add_for_job(
		num_jobs,
		std::forward<ResourceCallback>(resource_callback),
		std::forward<JobCallback>(job_callback),
		std::forward<PolicyCallback>(policy_callback),
		{});
}

template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
auto jobs::scheduler::add_for_job(
	size_t const num_jobs,
	ResourceCallback&& resource_callback,
	JobCallback&& job_callback,
	PolicyCallback&& policy_callback,
	std::vector<job_handle> const& dependencies) -> job_handle
{
	auto start_job = add_job([] {}, {}, dependencies);
	if(num_jobs == 0)
	{
		return start_job;
	}

	auto const num_threads = std::thread::hardware_concurrency();
	auto const num_batches = std::min(static_cast<size_t>(num_threads), num_jobs);
	auto const batch_size = num_jobs / num_batches;
	auto const last_batch_size = batch_size + num_jobs % num_batches;

	auto local_resource_access_map = std::unordered_multimap<uintptr_t, jobs::access_policy>{};
	auto job_handles = std::vector<job_handle>{};
	for(auto current_batch = size_t{0}; current_batch < num_batches; ++current_batch)
	{
		auto pair = process_jobs_in_batch(
			current_batch * batch_size,
			current_batch == num_batches - 1 ? last_batch_size : batch_size,
			resource_callback,
			policy_callback,
			job_callback,
			local_resource_access_map);
		auto&& callbacks = pair.first;
		auto&& batch_policies = pair.second;
		job_handles.emplace_back(add_job(
			[callbacks] { std::for_each(callbacks.begin(), callbacks.end(), [](auto&& callback) { callback(); }); },
			batch_policies,
			{start_job}));
	}

	auto end_job = add_job([] {}, {}, job_handles);
	return end_job;
}

template<typename ResourceCallback, typename PolicyCallback, typename JobCallback>
auto jobs::scheduler::process_jobs_in_batch(
	size_t job_offset,
	size_t batch_size,
	ResourceCallback&& resource_callback,
	PolicyCallback&& policy_callback,
	JobCallback&& job_callback,
	std::unordered_multimap<uintptr_t, access_policy>& access_map)
	-> std::pair<std::vector<std::function<void()>>, access_policy_container>
{
	// It might be possible to store the lambdas directly rather than as std::function,
	// but I'm not entirely sure how to do the decltype magic required for that.
	auto callbacks = std::vector<std::function<void()>>{};
	auto batch_policies = jobs::access_policy_container{};
	for(auto job_in_batch = size_t{0}; job_in_batch < batch_size; ++job_in_batch)
	{
		auto const current_job = job_offset + job_in_batch;
		auto&& resources = resource_callback(current_job);
		auto&& policies = policy_callback(resources);

		check_access_overlap_parallel_for(access_map, policies);
		std::for_each(
			policies.access_policies_.begin(),
			policies.access_policies_.end(),
			[&access_map, &batch_policies](auto&& pair) {
				access_map.emplace(pair);
				batch_policies.access_policies_.emplace_back(pair);
			});

		callbacks.emplace_back([job_callback, resource_callback, current_job, policies]() mutable {
			policies.reset_policies();
			policies.activate_policies();
			auto&& resources = resource_callback(current_job);
			job_callback(resources);
		});
	}
	return std::make_pair(callbacks, batch_policies);
}
