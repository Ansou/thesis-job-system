[requires]
catch2/2.12.1
nlohmann_json/3.7.3

[generators]
cmake_find_package
cmake_paths
