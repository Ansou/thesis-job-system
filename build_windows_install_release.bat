@echo off
cd %~dp0
mkdir build
cd build
conan install .. -s build_type=Release --build missing
cmake .. -DCMAKE_TOOLCHAIN_FILE=conan_paths.cmake -G "Visual Studio 16 2019" -A x64
cmake --build . --config release --target install
pause
