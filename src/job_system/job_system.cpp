#include <cassert>
#include <job_system/job_system.hpp>
#include <mutex>
#include <stack>
#include <stdexcept>
#include <unordered_set>


using namespace std::string_literals;

thread_local std::unordered_map<uintptr_t, jobs::access_policy> jobs::policy_storage::policies_ = {};

auto jobs::stage_handle::wait() const -> void
{
	end_job_handle_.wait();
}

jobs::stage_handle::stage_handle(job_handle end_job_handle) : end_job_handle_{end_job_handle} {}

auto jobs::access_policy::overlaps(access_policy const other) const -> bool
{
	return access_mode == access_mode::write &&
		(other.access_mode == access_mode::write || other.access_mode == access_mode::read) ||
		other.access_mode == access_mode::write && access_mode == access_mode::read;
}

auto jobs::policy_storage::reset_policies() -> void
{
	policies_.clear();
}

auto jobs::policy_storage::set_policy(uintptr_t ptr, access_policy policy) -> void
{
	policies_.emplace(ptr, policy);
}

auto jobs::access_policy_container::activate_policies() -> void
{
	std::for_each(access_policies_.begin(), access_policies_.end(), [](auto pair) {
		policy_storage::set_policy(pair.first, pair.second);
	});
}

auto jobs::access_policy_container::reset_policies() -> void
{
	policy_storage::reset_policies();
}

auto jobs::job_handle::operator<(job_handle const& other) const -> bool
{
	// return false;
	return previous_time_ < other.previous_time_;
}

auto jobs::job_handle::operator>(job_handle const& other) const -> bool
{
	// return false;
	return previous_time_ > other.previous_time_;
}

auto jobs::job_handle::wait() const -> void
{
	if(auto const job = get_job(); job)
	{
		job->wait();
		scheduler_->clean_jobs_recursively(*this);
	}
}

jobs::job_handle::job_handle(scheduler& scheduler, size_t id, std::chrono::nanoseconds previous_time) :
	scheduler_{&scheduler}, id_{id}, previous_time_{previous_time}
{
}

jobs::job::job(
	std::function<void()> callback,
	scheduler& scheduler,
	size_t id,
	std::vector<job_handle> const& dependencies,
	access_policy_container access_policies,
	std::chrono::nanoseconds previous_time,
	job_timer* timer) :
	callback_{callback},
	scheduler_{&scheduler},
	id_{id},
	dependencies_remaining_{static_cast<int>(dependencies.size())},
	dependencies_{dependencies},
	previous_time_{previous_time},
	timer_{timer},
	access_policies_{std::move(access_policies)}
{
}

auto jobs::job::finish() -> void
{
	auto finish_lock = std::scoped_lock{finish_mutex_};
	std::for_each(dependent_jobs_.begin(), dependent_jobs_.end(), [&](job* dependent) {
		dependent->decrement_remaining_dependencies();
	});
	has_finished_ = true;
	finish_time_ = std::chrono::steady_clock::now();
	finish_signaller_.notify_all();
}

auto jobs::job::decrement_remaining_dependencies() -> void
{
	if(--dependencies_remaining_ == 0)
	{
		push_to_queue();
	}
}

auto jobs::job::push_to_queue() const -> void
{
	{
		auto lock = std::scoped_lock{scheduler_->queue_mutex_};
		scheduler_->job_queue_.push(create_handle());
	}
	scheduler_->queue_cond_var_.notify_one();
}

auto jobs::job::create_handle() const -> job_handle
{
	return {*scheduler_, id_, previous_time_};
}

auto jobs::job::register_dependencies(std::vector<job_handle> const& dependencies) -> void
{
	if(dependencies.empty())
	{
		push_to_queue();
	}
	else
	{
		auto jobs_lock = std::shared_lock{scheduler_->jobs_mutex_};
		std::for_each(dependencies.begin(), dependencies.end(), [&](job_handle dependency) {
			auto job = dependency.get_job();
			if(!job)
			{
				decrement_remaining_dependencies();
				return;
			}
			auto finish_lock = std::unique_lock{job->finish_mutex_};
			if(job->has_finished_)
			{
				finish_lock.unlock();
				decrement_remaining_dependencies();
			}
			else
			{
				job->dependent_jobs_.push_back(this);
			}
		});
	}
}

auto jobs::job::dependencies() const -> std::vector<job_handle> const&
{
	return dependencies_;
}

auto jobs::job::policies() const -> access_policy_container const&
{
	return access_policies_;
}

auto jobs::job::wait() -> void
{
	auto finish_lock = std::unique_lock{finish_mutex_};
	finish_signaller_.wait(finish_lock, [&finished = has_finished_] { return finished; });
}

auto jobs::job::operator()() -> void
{
	if(timer_)
	{
		timer_->start();
	}
	access_policies_.activate_policies();
	callback_();
	access_policies_.reset_policies();
	if(timer_)
	{
		timer_->finish();
	}
}

auto jobs::job::operator==(job const& other) const -> bool
{
	return id_ == other.id_;
}

auto jobs::job_timer::start() -> void
{
	start_time_ = std::chrono::steady_clock::now();
}

auto jobs::job_timer::finish() -> void
{
	time_taken_ = std::chrono::steady_clock::now() - start_time_;
}

auto jobs::job_timer::time_taken() const -> std::chrono::steady_clock::duration
{
	return time_taken_;
}

jobs::stage_scheduler::stage_scheduler(scheduler& scheduler) : scheduler_{&scheduler} {}

auto jobs::stage_scheduler::add_job(
	std::function<void()> callback,
	std::chrono::steady_clock::duration previous_time,
	jobs::job_timer* timer,
	access_policy_container access_policies) -> job_handle
{
	return add_job(std::move(callback), previous_time, timer, std::move(access_policies), {});
}

auto jobs::stage_scheduler::add_job(
	std::function<void()> callback,
	std::chrono::steady_clock::duration previous_time,
	jobs::job_timer* timer,
	access_policy_container access_policies,
	std::vector<job_handle> const& dependencies) -> job_handle
{
	auto id = scheduler_->create_new_id();
	jobs_.emplace(
		id, job_data{std::move(callback), previous_time, timer, dependencies, id, std::move(access_policies)});
	handles_.push_back({*scheduler_, id, previous_time});
	return {*scheduler_, id, previous_time};
}

auto jobs::stage_scheduler::schedule_jobs(stage_handle* previous_stage) -> stage_handle
{
	auto queue_lock = std::scoped_lock{scheduler_->queue_mutex_};
	for(auto& pair : jobs_)
	{
		auto&& data = pair.second;
		auto dependencies = data.dependencies;
		if(previous_stage)
		{
			dependencies.push_back(previous_stage->end_job_handle_);
		}
		scheduler_->schedule_job(
			std::move(data.callback),
			data.id,
			dependencies,
			std::move(data.access_policies),
			data.previous_time,
			data.timer);
	}

	auto const end_job_handle = scheduler_->add_job([] {}, {}, handles_);

	return stage_handle{end_job_handle};
}

auto jobs::scheduler::add_job(std::function<void()> callback, access_policy_container access_policies) -> job_handle
{
	return add_job(std::move(callback), std::move(access_policies), {});
}

auto jobs::scheduler::add_job(
	std::function<void()> callback,
	access_policy_container access_policies,
	std::vector<job_handle> const& dependencies) -> job_handle
{
	auto const id = create_new_id();
	auto& new_job = schedule_job(std::move(callback), id, dependencies, access_policies, {}, nullptr);
	return new_job.create_handle();
}

auto jobs::scheduler::execute_next_job(std::chrono::nanoseconds wait_timeout) -> bool
{
	auto queue_lock = std::unique_lock{queue_mutex_};
	if(!queue_cond_var_.wait_for(queue_lock, wait_timeout, [&queue = job_queue_] { return !queue.empty(); }))
	{
		return false;
	}

	auto const next_job_handle = job_queue_.top();
	job_queue_.pop();
	queue_lock.unlock();

	auto jobs_lock = std::shared_lock{jobs_mutex_};
	auto& next_job = *next_job_handle.get_job();
	jobs_lock.unlock();
	next_job();
	next_job.finish();

	return true;
}

auto jobs::scheduler::clean_jobs_recursively(job_handle root_job) -> void
{
	auto jobs_to_clean = find_recursive_dependencies({root_job});
	auto unique_jobs_lock = std::scoped_lock{jobs_mutex_};
	std::for_each(jobs_to_clean.begin(), jobs_to_clean.end(), [this](auto id) { remove_job(id); });
}

auto jobs::scheduler::remove_job(size_t id) -> void
{
	auto iter = all_jobs_.find(id);
	if(iter == all_jobs_.end())
	{
		return;
	}
	auto&& job = iter->second;
	job.wait();
	if constexpr(enable_access_overlap_checks)
	{
		std::for_each(
			job.policies().access_policies_.begin(), job.policies().access_policies_.end(), [this, &job](auto&& pair) {
				remove_job_policy_from_access_map(job, pair.first);
			});
	}
	all_jobs_.erase(iter);
}

auto jobs::scheduler::remove_job_policy_from_access_map(job const& job, uintptr_t const resource) -> void
{
	auto const iter_pair = resource_access_map_.equal_range(resource);
	for(auto iter = iter_pair.first; iter != iter_pair.second; ++iter)
	{
		if(iter->second.first == &job)
		{
			resource_access_map_.erase(iter);
			break;
		}
	}
}

auto jobs::scheduler::check_access_overlap_parallel_for(
	std::unordered_multimap<uintptr_t, jobs::access_policy> const& resource_access_map,
	jobs::access_policy_container const& policies) -> void
{
	if constexpr(enable_access_overlap_checks)
	{
		for(auto&& policy_pair : policies.access_policies_)
		{
			auto matching_jobs_range = resource_access_map.equal_range(policy_pair.first);
			for(auto matching_job_iter = matching_jobs_range.first; matching_job_iter != matching_jobs_range.second;
				++matching_job_iter)
			{
				auto&& other_policy = matching_job_iter->second;
				if(policy_pair.second.overlaps(other_policy))
				{
					assert(false);
				}
			}
		}
	}
}

auto jobs::scheduler::create_new_id() -> size_t
{
	return ++current_id_;
}

auto jobs::scheduler::schedule_job(
	std::function<void()>&& callback,
	size_t id,
	std::vector<job_handle> const& dependencies,
	jobs::access_policy_container const& access_policies,
	std::chrono::nanoseconds previous_time,
	job_timer* timer) -> job&
{
	check_access_overlaps(dependencies, access_policies);

	auto jobs_lock = std::unique_lock{jobs_mutex_};
	auto const result =
		all_jobs_.try_emplace(id, callback, *this, id, dependencies, access_policies, previous_time, timer);
	if(!result.second)
	{
		throw std::runtime_error{"Could not place new job in scheduler."s};
	}
	auto& job = result.first->second;
	jobs_lock.unlock();
	job.register_dependencies(dependencies);

	if constexpr(enable_access_overlap_checks)
	{
		std::for_each(
			access_policies.access_policies_.begin(),
			access_policies.access_policies_.end(),
			[&access_map = resource_access_map_, &job](auto&& pair) {
				access_map.emplace(pair.first, std::make_pair(&job, pair.second));
			});
	}

	return job;
}

auto jobs::scheduler::check_access_overlaps(
	std::vector<job_handle> const& dependencies, jobs::access_policy_container const& access_policies) -> void
{
	if constexpr(enable_access_overlap_checks)
	{
		auto jobs_lock = std::shared_lock{jobs_mutex_};
		auto const visited_jobs = find_recursive_dependencies(dependencies);
		for(auto&& policy_pair : access_policies.access_policies_)
		{
			auto matching_jobs_range = resource_access_map_.equal_range(policy_pair.first);
			for(auto matching_job_iter = matching_jobs_range.first; matching_job_iter != matching_jobs_range.second;
				++matching_job_iter)
			{
				auto&& other_job = *matching_job_iter->second.first;
				auto&& other_policy = matching_job_iter->second.second;
				check_access_overlap_against_job(policy_pair.second, other_policy, other_job, visited_jobs);
			}
		}
	}
}

auto jobs::scheduler::check_access_overlap_against_job(
	jobs::access_policy const policy,
	jobs::access_policy const other_policy,
	job const& other_job,
	std::vector<size_t> const& safe_jobs) const -> void
{
	if(policy.overlaps(other_policy))
	{
		if(auto const visited_jobs_find_iter =
			   std::find(safe_jobs.begin(), safe_jobs.end(), other_job.create_handle().id_);
		   visited_jobs_find_iter == safe_jobs.end())
		{
			assert(false);
		}
	}
}

auto jobs::scheduler::find_recursive_dependencies(std::vector<job_handle> const& dependencies) const
	-> std::vector<size_t>
{
	auto visited_jobs = std::vector<size_t>{};
	auto stack = std::stack<job_handle>{};
	for(auto handle : dependencies)
	{
		stack.push(handle);
		while(!stack.empty())
		{
			auto s = stack.top();
			stack.pop();

			if(std::find(visited_jobs.begin(), visited_jobs.end(), s.id_) == visited_jobs.end())
			{
				visited_jobs.emplace_back(s.id_);
			}
			push_dependencies_to_stack(s, stack, visited_jobs);
		}
	}
	return visited_jobs;
}

auto jobs::scheduler::push_dependencies_to_stack(
	job_handle const job, std::stack<job_handle>& stack, std::vector<size_t> const& visited_jobs) const -> void
{
	if(auto const iter = all_jobs_.find(job.id_); iter != all_jobs_.end())
	{
		for(auto&& dep : iter->second.dependencies())
		{
			if(std::find(visited_jobs.begin(), visited_jobs.end(), dep.id_) == visited_jobs.end())
			{
				stack.push(dep);
			}
		}
	}
}

auto jobs::job_handle::get_job() const -> job*
{
	if(auto iter = scheduler_->all_jobs_.find(id_); iter != scheduler_->all_jobs_.end())
	{
		return &iter->second;
	}
	return nullptr;
}

jobs::thread_pool::thread_pool(scheduler& scheduler, std::chrono::nanoseconds queue_wait_timeout, int num_threads)
{
	assert(num_threads > 0);
	for(auto i = 0; i < num_threads; ++i)
	{
		threads_.emplace_back([&scheduler, queue_wait_timeout, &running = running_] {
			while(scheduler.execute_next_job(queue_wait_timeout) || running)
			{
			}
		});
	}
}

jobs::thread_pool::~thread_pool()
{
	stop();
}

auto jobs::thread_pool::stop() -> void
{
	running_ = false;
	std::for_each(threads_.begin(), threads_.end(), [](auto& thread) {
		if(thread.joinable())
			thread.join();
	});
}
